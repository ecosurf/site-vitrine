import React from 'react';

import Header from '../../components/header';
import Footer from '../../components/footer';
import SectionContribution from '../../components/section-contribution';
import dashboard from './dashboard.png';

const Analyse = () => {

    return (
        <section>
            <Header />
            <section className='sm:flex sm:justify-between my-10 max-w-xs sm:max-w-5xl mx-auto'>
                <div>
                    <div className='text-3xl font-bold leading-normal tracking-wider'>
                        <div>La pollution numérique, </div>
                        <div>l'affaire de tous</div>
                    </div>
                    <div className="divider my-5"></div>
                    <div className='text-center text-xl font-bold my-10 max-w-sm'>
                        Le numérique est responsable de <a className="underline underline-offset-2 decoration-0" href="https://theshiftproject.org/article/climat-insoutenable-usage-video/">4 % des émissions mondiales de CO2</a>, produisant <span className="primary-color"><a className="underline underline-offset-2 decoration-0" title="40,6 milliard de tonnes de CO2 sont émises par an dans le monde, dont 4 % dues au numérique" href="https://www.lemonde.fr/climat/article/2022/11/11/les-emissions-mondiales-de-co2-restent-a-des-niveaux-record-cette-annee_6149409_1652612.html">1 624 millions de tonnes de CO₂ par an</a>*</span> dans le monde
                    </div>

                    <div className='text-center text-xl font-bold my-10 max-w-sm'>
                        La pollution numérique <span className="primary-color"><a className="underline underline-offset-2 decoration-0" href="https://www.lesnumeriques.com/vie-du-net/le-numerique-pourrait-representer-7-de-nos-emissions-de-gaz-a-effet-de-serre-d-ici-2040-n151739.html">doublera</a></span> d'ici <span className="primary-color">2025</span>.
                    </div>

                </div>

                <img className="mx-auto my-2 w-60 h-60 sm:h-80 sm:w-80 sm:m-none" src={process.env.PUBLIC_URL + '/pictures/data-analyse.jpg'} alt="data-analyse" />

            </section>

            <div className="my-10 mt-20 max-w-xs sm:max-w-5xl mx-auto text-xs"><span className="primary-color">
                *</span>Il faudrait faire <a className="underline underline-offset-2" title="Une voiture de grande distribution comme une Renault Megane consomme 125g CO₂/km" href="https://carlabelling.ademe.fr/fiche/2233/renault-megane%20berline%20115ch">325 millions de fois le tour de la Terre en voiture</a> pour égaliser.
            </div>

            <section className='my-10 mt-20 max-w-xs sm:max-w-5xl mx-auto'>
                <div className='text-3xl font-bold leading-normal tracking-wider'>
                    Le numérique : un secteur polluant
                </div>
                <div className="divider my-5"></div>
                <div>
                    D'après <a className="underline underline-offset-2" href="https://www.arcep.fr/la-regulation/grands-dossiers-thematiques-transverses/lempreinte-environnementale-du-numerique.html">un rapport de l'Autorité de Régulation des Communications Électroniques, des Postes et de la distribution de la presse</a> (Arcep), le numérique compte pour <span className="font-bold">2,5 % de l'empreinte carbone nationale</span>. Cette part devrait augmenter de 60 % d'ici à 2040, pour atteindre ainsi <span className="font-bold">6,4 % de l'empreinte carbone nationale</span>, si rien n'est fait pour ralentir cette progression.
                </div>
                <br />
                <div>
                    Si 70 % de cette empreinte est imputable à la fabrication et à l'utilisation des terminaux, les 30 % restants proviennent du réseau et des serveurs. Ces émissions peuvent être limitées, par exemple en privilégiant l'utilisation de services dont les serveurs sont <span className="font-bold">géographiquement proches de l'utilisateur final</span>, et alimentés par une <span className="font-bold">électricité décarbonée</span>.
                </div>
            </section>

            <section className='my-10 mt-20 max-w-xs sm:max-w-5xl mx-auto'>
                <div className='text-3xl font-bold leading-normal tracking-wider'>
                    Loi numérique décarboné du 25 octobre 2023
                </div>
                <div className="divider my-5"></div>
                <div>
                    <span className="text-xl font-bold">Que dit la loi ?</span><br />
                    L'Assemblée Nationale et le Sénat ont adopté le 25 octobre 2023 une nouvelle loi, dite “numérique décarboné”, <span className="font-bold">limitant la masse de CO₂ équivalent</span> que tout un chacun est autorisé à rejeter dans l'atmosphère chaque mois par son utilisation d'internet à des fins personnelles <span className="font-bold">à 3 kg</span>. Au delà, la consommation sera sanctionnée d'une <span className="font-bold">amende</span>.
                    Le calcul des émissions est à la charge des fournisseurs d'accès à internet (FAI), via l'utilisation des solutions d'<span className="primary-color font-bold">EcoSurf</span>.
                </div>
                <br />
                <div>
                    <span className="text-xl font-bold">Quelle quantité de donnée pour vos clients ?</span><br />
                    Pour respecter nos engagements climatique et maintenir le réchauffement climatique en deçà de 2°C d'ici à la fin du siècle, chacun devra émettre <span className="font-bold"><a className="underline underline-offset-2" href="https://www.2tonnes.org/">au maximum 2 tonnes de CO₂ équivalent par an</a> dans l'atmosphère</span>. L'utilisation d'internet compte actuellement pour 1,2 % de l'empreinte carbone d'un citoyen français. Cette même proportion appliquée aux 2 tonnes par an donne 2 kg de CO₂ équivalent par mois. Pour accompagner l'utilisation croissante du numérique, tout en évitant la sur-consommation déraisonnée, la loi prévoit 3 kg par mois.<br />
                    3 kg de CO₂ équivalent correspondent à la consommation, <span className="font-bold">en France</span>, d'<span className="font-bold"><a className="underline underline-offset-2" href="https://expertises.ademe.fr/economie-circulaire/consommer-autrement/passer-a-laction/reconnaitre-produit-plus-respectueux-lenvironnement/dossier/laffichage-environnemental/affichage-environnemental-secteur-numerique">environ 60 giga-octets</a></span>. En revanche, si la consommation est réalisée au travers de <span className="font-bold">serveurs allemands</span>, alimentés par une électricité carbonée, seuls environ <span className="font-bold">6 giga-octets</span> pourront être consommés.
                </div>
                <br />
                <div>
                    <span className="text-xl font-bold">Cette loi peut-elle être efficace ?</span><br />
                    On peut se demander si faire payer des amendes en cas de dépassement du forfait de 3 kg de CO₂ équivalent permettra de changer les habitudes de consommation. Il peut alors être intéressant de considérer des cas concrets où la sanction financière a été appliquée pour changer les comportements. Les taxes sur le tabac et l'alcool en France par exemple ont fait baisser la consommation, comme l'indique un <a className="underline underline-offset-2" href="https://www.senat.fr/rap/r13-399/r13-3994.html">rapport du Sénat du 26 février 2014</a>. Ainsi, une hausse de 10 % des prix entraîne en moyenne une baisse de 4,8 % de la consommation de tabac, de 4,6 % pour la bière, 6,9 % pour le vin et jusqu'à 8 % pour les spiritueux. Le rapport insiste longuement sur la répartition de ces diminutions au sein des différentes catégories de consommateurs. Néanmoins, la tendance est sans appel : <span className="font-bold">l'augmentation du prix</span>, ici par une taxation croissante, <span className="font-bold">entraîne une baisse de la consommation</span>.
                </div>
            </section>

            <section className='my-10 mt-20 max-w-xs sm:max-w-5xl mx-auto'>
                <div className='text-3xl font-bold leading-normal tracking-wider'>
                    Les solutions d'EcoSurf
                </div>
                <div className="divider my-5"></div>
                <div>
                    En tenant compte du pays dans lequel est hébergé un serveur, de son efficacité énergétique et de la distance qui le sépare de son utilisateur, EcoSurf calcule les émissions de CO₂ équivalent dudit utilisateur.
                </div>
                <br />
                <div>
                    <span className="text-xl font-bold">Pour le client : un système simple</span><br />
                    Chaque client peut visualiser simplement, et de la même manière quel que soit son FAI, la quantité de CO₂ équivalent rejetée au cours du mois.
                    <img src={dashboard} alt="Tableau de bord permettant de visualiser sa consommation de CO₂ équivalent rejetée par l'utilisation d'internet" />
                </div>
                <br />
                <div className="mt-15">
                    <span className="text-xl font-bold">En cas de dépassement ?</span><br />
                    Les clients sont prévenus en franchissant les paliers de 2 kg, 2,5 kg et 3 kg. Toutefois, en cas de dépassement des 3 kg autorisés, ils <span className="font-bold">paient une amende</span> directement envoyée à leur domicile, fonction exponentielle de la quantité excessive de CO₂ équivalent rejetée par le client, pondérée par 5 % de ses revenus moyens des 12 derniers mois.
                </div>
                <br />
                <div>
                    Par exemple, si vous avez gagné en moyenne 2 500 € par mois sur les 12 derniers mois et que vous avez dépensé pour 4 kg de CO₂ équivalent par votre utilisation d'internet, vous paierez 340 € d'amende. Pour 5 kg de CO₂ équivalent, ce sera 924 €, et pour 6 kg, 2 511 €.
                </div>
            </section>

            <SectionContribution />
            <Footer />
        </section>
    )
}

export default Analyse;