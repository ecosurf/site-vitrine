import React from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';
import SectionContribution from '../../components/section-contribution';

import './index.css'

const Marketing = () => {

    return (
        <section>
            <Header />
            <section className='sm:flex sm:justify-between my-10 max-w-xs sm:max-w-5xl mx-auto'>
                <div>
                    <div className='text-3xl font-bold leading-normal tracking-wider'>
                        <span>Mesurez le </span> <span class="primary-color">CO₂</span> <div>émis par vos clients</div>
                    </div>
                    <div className="divider my-5"></div>
                    <div className="w-4/5">
                        La France impose désormais que les fournisseurs d'accès à internet (FAI), comme Orange, Bouygues Telecom... limitent à <span className="font-bold">3 kilogrammes de CO₂</span> par mois les émissions de leurs clients liées à leur utilisation d'internet. À cette fin, elle impose aux FAI d'utiliser les solutions libres développées par <span class="primary-color font-bold">EcoSurf</span> pour mesurer l'empreinte carbone de leurs clients.
                    </div>
                </div>

                <img className="mx-auto my-2 w-60 h-60 sm:h-80 sm:w-80 sm:m-none" src={process.env.PUBLIC_URL + '/pictures/data-marketing.jpg'} alt="data-marketing" />
            </section>

            <section className='my-10 max-w-xs sm:max-w-5xl mx-auto'>
                <div className='text-3xl font-bold leading-normal tracking-wider'>
                    Contexte
                </div>
                <div className="divider my-5"></div>
                <div className='sm:flex sm:justify-around text-center my-10'>
                    <div className='mx-auto my-10 sm:my-2'>
                        <div className="my-2 text-2xl font-bold text-center">Loi <span className="primary-color">"numérique décarboné"</span>
                            <br />
                            du 25 octobre 2023</div>
                    </div>

                    <div className='mx-auto my-10 sm:my-2'>
                        <div className="my-2 text-2xl font-bold text-center">Seuil par personne de</div>
                        <div className='primary-color text-2xl font-bold text-center'>3 kg CO₂ équivalent par mois</div>
                    </div>
                </div>
                <div>
                    Les FAI sont désormais <span className="font-bold">obligés de suivre la consommation carbone</span> de leurs clients et de <span className="font-bold">facturer l'excédent</span> au compte de l'État.
                </div>
                <br />
                <div>
                    Les navigateurs et moteurs de recherches doivent également afficher un <span className="font-bold">indicateur de pollution numérique pour chaque site</span> afin de guider les utilisateurs.
                </div>
            </section>

            <section className='my-10 max-w-xs sm:max-w-5xl mx-auto'>
                <div className='text-3xl font-bold leading-normal tracking-wider'>
                    Nos services
                </div>
                <div className="divider my-5"></div>
                <div className='sm:flex sm:justify-around text-center my-10'>
                    <div className='w-44 mx-auto my-2'>
                        <img className="w-16 h-16 m-auto" src={process.env.PUBLIC_URL + '/pictures/icon-monitoring.png'} alt="icon-monitoring" />
                        <div className="my-2 text-center">Visualisez la consommation en <strong>CO₂</strong> de vos clients</div>
                    </div>

                    <div className='w-44 mx-auto my-2'>
                        <img className="w-16 h-16 m-auto" src={process.env.PUBLIC_URL + '/pictures/icon-target.png'} alt="icon-target" />
                        <div className="my-2 text-center">Identifiez les usages les plus économes pour guider vos utilisateurs</div>
                    </div>

                    <div className='w-44 mx-auto my-2'>
                        <img className="w-16 h-16 m-auto" src={process.env.PUBLIC_URL + '/pictures/icon-compute.png'} alt="icon-compute" />
                        <div className="my-2 text-center">Amende automatique en cas de dépassement du seuil, pour l'État</div>
                    </div>
                </div>
            </section>

            <SectionContribution />

            <Footer />
        </section>
    )
}

export default Marketing;