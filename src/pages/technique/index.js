import React from 'react';

import Header from '../../components/header';
import Footer from '../../components/footer';
import SectionContribution from '../../components/section-contribution';

import './index.css';

const Technique = () => {

    return (
        <section>
            <Header />

            <section className='sm:flex sm:justify-between my-10 max-w-xs sm:max-w-5xl mx-auto'>
                <div>
                    <div className='text-3xl font-bold leading-normal tracking-wider'>
                        <div>Des solutions</div>
                        <div>clés en main</div>
                    </div>
                    <div className="divider my-5"></div>
                    <div className="w-3/5">
                        Nos solutions permettent aux FAI et aux utilisateurs de respecter la <span class="primary-color font-bold">loi du numérique décarbonée</span> en limitant la pollution numérique émise par nos usages.
                    </div>
                </div>

                <img className="mx-auto my-2 w-60 h-60 sm:h-80 sm:w-80 sm:m-none" src={process.env.PUBLIC_URL + '/pictures/data-technique.jpg'} alt="data-technique" />
            </section>

            <section className='my-10 max-w-xs sm:max-w-5xl mx-auto'>
                <div className='text-3xl font-bold leading-normal tracking-wider'>
                    Prérequis
                </div>
                <div className="divider my-5"></div>

                <div>
                    Pour pouvoir réaliser nos solutions techniques, les entreprises doivent fournir leurs données exactes de consommation de CO₂. Afin que nous puissions évaluer le coût exacte d'une donnée entre son stockage et son arrivée chez l'utilisateur.*                </div>
                <br />
                <div className='font-light text-sm'>
                    * seulement 40% des entreprises sont prêtes à fournir ces données selon l'enquête Bluenove-BVA
                </div>

                <div className='my-5'>

                    <img className="mx-auto my-4 w-4/6" src={process.env.PUBLIC_URL + '/pictures/schema-technique.png'} alt="schema-technique" />

                    <div className='font-light text-xs text-center'>
                        Schématisation du problème
                    </div>
                </div>

            </section>

            <section className='my-10 max-w-xs sm:max-w-5xl mx-auto'>
                <div className='text-3xl font-bold leading-normal tracking-wider'>
                    Comment EcoSurf fonctionne ?
                </div>
                <div className="divider my-5"></div>

                <div>
                    Pour mettre en place un seuil de consommation CO₂ numérique par personne, il faut <strong>appliquer un traitement sur chaque requête Internet d'un utilisateur</strong> afin de calculer sa consommation en CO₂. Cette action est possible par les <strong>Fournisseurs d'Accès Internet (Orange, Bouygues, ...) qui ont déjà accès à l'ensemble des requêtes</strong> de leurs clients.
                </div>
                <br />
                <div>
                    EcoSurf vient alors avec une solution permettant aux FAI d'analyser la consommation de leurs clients et aux utilisateurs de visualiser leur suivi d'activité à travers un tableau de bord.
                </div>

                <img className="mx-auto my-4 w-4/6" src={process.env.PUBLIC_URL + '/pictures/dashboard.png'} alt="dashboard" />

                <div className='font-light text-xs text-center'>
                    Extrait du tableau de bord des clients d'un FAI
                </div>

                <div className='my-10'>
                    Pour faciliter la navigation des utilisateurs sur Internet et leur permettre de <strong>prendre les choix les plus économes en terme de rejet de gaz à effets de serre</strong>, nous proposons un <strong>indicateur de pollution numérique (CO₂/secondes)</strong> que les moteurs de recherche pourront afficher à côté de chaque site Internet afin que les utilisateurs privilégie le choix d'un site plus économes selon leur besoin.                </div>

                <img className="mx-auto my-4 w-2/6" src={process.env.PUBLIC_URL + '/pictures/indicateur-pollution.png'} alt="indicateur-pollution" />

                <div className='font-light text-xs text-center'>
                    Exemple d'indicateur de pollution numérique
                </div>

                <div className='my-10'>
                    De plus, les utilisateurs peuvent également installer une extension à leur navigateur afin de visualiser leur consommation en direct et savoir si leur seuil de consommation de CO₂ est bientôt dépassé.
                </div>

                <img className="mx-auto my-4 w-2/6" src={process.env.PUBLIC_URL + '/pictures/extension.png'} alt="extension" />

                <div className='font-light text-xs text-center'>
                    Exemple de l'extension EcoSurf
                </div>

            </section>

            <section className='my-10 max-w-xs sm:max-w-5xl mx-auto'>
                <div className='text-3xl font-bold leading-normal tracking-wider'>
                    Calcul de la consommation CO₂
                </div>
                <div className="divider my-5"></div>

                <div>
                    Pour calculer la <strong>consommation carbone</strong> de chacun selon son traffic, nous avons besoin d'informations sur :
                    <ul className='list-disc list-inside'>
                        <li>Le coût de <strong>stockage et traitement des données</strong> avec la consommation des serveurs internes des- entreprises (ratio CO₂ / Go calculé par les entreprises )</li>
                        <li>Le coût de <strong>transferts des données</strong> avec la consommation des réseaux utilisés pour l'échange des données</li>
                    </ul>
                </div>
            </section>

            <div className='bg-primary-color text-white py-5'>
                <section className='sm:flex sm:justify-between my-10 max-w-xs sm:max-w-5xl mx-auto'>
                    <div>
                        <p className='font-semibold my-4'>Règle de calcul : </p>
                        <div>
                            <div className='font-light font-math text-center'>
                                (Coût carbone propre au site utilisé + coût carbone du réseau) * quantité de données = quantité de CO₂
                            </div>
                            <div className='font-light font-math text-center my-4'>
                                (kg de CO₂ par Go) * Go   =  kg de CO₂
                            </div>
                            <div>
                                Le coût en carbone du réseau est calculé en sommant les coûts energétiques de chaque serveur pondéré par la coût carbone de l'énérgie dans le pays où le serveur est localisé :
                            </div>
                            <div className='font-light font-math text-center my-4'>
                                Σ   Watt par Go * kg de CO₂ par Watt  = kg de  CO₂ par Go
                            </div>
                        </div>

                    </div>
                </section>
            </div >

            <section className='sm:flex sm:justify-between my-10 max-w-xs sm:max-w-5xl mx-auto'>

                <section className='max-w-xs sm:max-w-5xl'>
                    <div className='text-3xl font-bold leading-normal tracking-wider'>
                        Sources du projet
                    </div>
                    <div className="divider my-5"></div>
                    <div>
                        <div className='my-4'>
                            <p className='font-semibold'>Site vitrine :</p>
                            <ul className='list-disc list-inside'>
                                <li>Disponible sur : <a href="https://intensif05.ensicaen.fr/">https://intensif05.ensicaen.fr/</a></li>
                                <li>Source présente sur : <a href="https://gitlab.com/ecosurf/site-vitrine">https://gitlab.com/ecosurf/site-vitrine</a></li>
                                <li>Technologies : ReactJS</li>
                            </ul>
                        </div>

                        <div className='my-4'>
                            <p className='font-semibold'>Tableau de bord  :</p>
                            <ul className='list-disc list-inside'>
                                <li>Disponible sur : <a href="https://intensif05.ensicaen.fr:8443/dashboard">https://intensif05.ensicaen.fr:8443/dashboard</a></li>
                                <li>Source présente sur : <a href="https://gitlab.com/ecosurf/front/ecosurf_app">https://gitlab.com/ecosurf/front/ecosurf_app</a></li>
                                <li>Technologies : ReactJS</li>
                            </ul>
                        </div>

                        <div className='my-4'>
                            <p className='font-semibold'>API de gestion de la consommation :</p>
                            <ul className='list-disc list-inside'>
                                <li>Disponible sur : <a href="https://intensif05.ensicaen.fr/consomanager/api/">https://intensif05.ensicaen.fr/consomanager/api/</a></li>
                                <li>Source présente sur : <a href="https://gitlab.com/ecosurf/back/ConsoManager">https://gitlab.com/ecosurf/back/ConsoManager</a></li>
                                <li>Technologies : NestJS</li>
                            </ul>
                        </div>

                        <div className='my-4'>
                            <p className='font-semibold'>API de gestion des utilisateurs :</p>
                            <ul className='list-disc list-inside'>
                                <li>Disponible sur : <a href="https://intensif05.ensicaen.fr/usermanager/api/user/current">https://intensif05.ensicaen.fr/usermanager/api/</a></li>
                                <li>Source présente sur : <a href="https://gitlab.com/ecosurf/back/usermanager">https://gitlab.com/ecosurf/back/usermanager</a></li>
                                <li>Technologies : ExpressJS</li>
                            </ul>
                        </div>

                        <div className='my-4'>
                            <p className='font-semibold'>Extension :</p>
                            <ul className='list-disc list-inside'>
                                <li>Indisponible en ligne</li>
                                <li>Source présente sur : <a href="https://gitlab.com/ecosurf/extension/chrome-extension/-/tree/main/extension">https://gitlab.com/ecosurf/extension/chrome-extension/-/tree/main/extension</a> </li>
                                <li>Technologies : Javascript Natif</li>
                            </ul>
                        </div>

                        <div className='my-4'>
                            <p className='font-semibold'>Simulation de navigation :</p>
                            <ul className='list-disc list-inside'>
                                <li>Indisponible en ligne</li>
                                <li>Source présente sur :  <a href="https://gitlab.com/ecosurf/extension/chrome-extension/-/tree/main/fake_site">https://gitlab.com/ecosurf/extension/chrome-extension/-/tree/main/fake_site</a>  </li>
                                <li>Technologies : Javascript Natif</li>
                            </ul>
                        </div>


                    </div>
                </section>
            </section>


            <SectionContribution />
            <Footer />
        </section >
    )
}

export default Technique;