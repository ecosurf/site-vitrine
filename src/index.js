import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './index.css';
import Marketing from './pages/marketing';
import Analyse from './pages/analyse';
import Technique from './pages/technique';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Marketing />} />
      <Route path="/analyse" element={<Analyse />} />
      <Route path="/technique" element={<Technique />} />
    </Routes>
  </BrowserRouter>
);
