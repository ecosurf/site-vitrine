import React from 'react';
import './index.css';

const SectionContribution = () => {

    return (
        <section className='mt-10 h-64 contribution-section'>
            <div className='pt-14 text-2xl font-bold text-center'>
                Contribuez à la transition écologique
            </div>
            <div className="divider m-auto my-5"></div>
            <div className='text-center my-10'>
                <button className="rounded bg-primary-color py-2 px-10 mx-auto text-white font-semibold">Testez-nous</button>
            </div>
        </section>
    )
}

export default SectionContribution;