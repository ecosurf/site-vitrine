import React from 'react';
import './index.css';

const Header = () => {

    const actif = window.location.pathname;

    return (
        <section className="sm:flex sm:justify-around my-6">
            <div>
                <a href="/">
                    <img className='w-20 h-10' alt="logo EcoSurf" src={`${process.env.PUBLIC_URL + "/pictures/logo.png"}`} />
                </a>
            </div>
            <div className="flex justify-around">
                <a className={`sm:mx-12 subtitle ${actif === '/' && 'link-active'}`} href="/">Accueil</a>
                <a className={`sm:mx-12 subtitle ${actif === '/analyse' && 'link-active'}`} href="/analyse">Analyse</a>
                <a className={`sm:mx-12 subtitle ${actif === '/technique' && 'link-active'}`} href="/technique">Technique</a>
            </div>
        </section>
    )
}

export default Header;