import React from 'react';

const Footer = () => {

    return (
        <footer class="flex justify-center bg-primary-color py-10">
            <div className="text-center text-white">Réalisé par <strong>EcoSurf</strong></div>
            <a href="https://gitlab.com/ecosurf/site-vitrine">
                <img className="h-8 w-8 absolute right-2.5" src={`${process.env.PUBLIC_URL + "/pictures/icon-github.png"}`} alt="icon-github" />
            </a>
        </footer>
    )
}

export default Footer;