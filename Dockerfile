FROM node:16-alpine as builder 

WORKDIR /app

COPY package*.json ./

COPY . .

RUN npm install -g serve

RUN npm install

RUN npm run build

EXPOSE 4200

CMD ["serve", "-s", "build", "-l", "4200"]